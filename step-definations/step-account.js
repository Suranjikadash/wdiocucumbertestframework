const{Given,When,Then}=require('@wdio/cucumber-framework');
// const NFTpage=require('./pages/nfturlpage');
const accountpage=require('../pages/accountpage');

Given(/^Open the browser and select account$/, async function() {
    // await NFTpage.open();
    await browser.url("https://nftmall-uat.netlify.app/");
    await browser.maximizeWindow();
    await browser.refresh();
    await browser.pause(8000);
    const handles=await browser.getWindowHandles();
    console.log (handles);
    await browser.switchToWindow(handles["1"]);
    const password = await $("//input[@id='password']");
    await password.setValue("123456788");
    const unlock = await $("//button[@class='button btn--rounded btn-default']");
    await unlock.click();        
    await browser.pause(4000);
    await browser.switchToWindow(handles["0"]);
         
    await accountpage.clickaccount();
});
When(/^select my Wallet$/, async function() {
    await accountpage.clickmyWallet();
});
When(/^select my Transactions$/, async function() {
    await accountpage.clickmyTransactions();
    await browser.pause(6000);
});    
When(/^select my Collections$/, async function() {
    await accountpage.clickmyCollections();
}); 
When(/^select my Favorites$/, async function() {
    await accountpage.clickmyFavouritess();
});
Then(/^select Document$/, async function() {
    await accountpage.clickDocument();
    await browser.debug(); 
});  


