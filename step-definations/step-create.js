const{Given,When,Then}=require('@wdio/cucumber-framework');
// const NFTpage=require('./pages/nfturlpage');
const createpage=require('../pages/createpage');

Given(/^Open the browser and select create$/, async function() {
    // await NFTpage.open();
    await browser.url("https://nftmall-uat.netlify.app/");
    await browser.pause(4000);
    await createpage.clickCreate();
    await browser.pause(4000);
});
When(/^select create single NFT$/, async function() {
    await createpage.clickCreateNow();
    
});
When(/^enter title and author name$/, async function() {
    await createpage.Entertitle("Sunshine");
    await createpage.EnterAuthorname("Moonlight");    
    
});
When(/^enter price$/, async function() {
    await createpage.EnterPrice(".002");
    
});
When(/^choose category$/, async function() {
    await createpage.SelectChooseCategory("Art");      
    
});
When(/^choose file to upload$/, async function() {
    await createpage.upload();      
    
});
When(/^write description$/, async function() {
    await createpage.Writedescription(`this is a canvas art\nKrishna with flute\noilpainting`);      
    
});
When(/^give royalty amount$/, async function() {
    await createpage.Royltyamountcheckboxclick();
    await createpage.Royltyamount("2");
    
});
When(/^add attributes$/, async function() {
    await createpage.clickAddAttributes();
    await createpage.EneterProperties1("height of canvas in cm");
    await createpage.Entervalue1("30");
    await createpage.clickAdd();
    await createpage.EneterProperties2("Weiht of canvas in kg");
    await createpage.Entervalue2("3")
    
});
Then(/^select Create$/, async function() {
    await createpage.clickCreate();   
});    
