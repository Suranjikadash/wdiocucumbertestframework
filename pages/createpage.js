const basepge = require("./basepage")
class createpage extends basepge{
    //LOCATORS
    get Create(){
        return $("//button[contains(text(),'Create')]");
    }
    get CreateNow(){
        return $("//*[@id='root']/header/div/center[2]/div[2]/div[2]/button");
    }
    get Title(){
        return $("//input[@name='title']");
    }
    get Authorname(){
        return $("//input[@name='authorname']");
    }
    get Price(){
        return $("//input[@name='price']");
    }
    get Choosecategory(){
        return $("//select[@name='category']");
    }    
    get Choosefile(){
        return $("//input[@class='form-control text-muted']");
    }  
    get Description(){
        return $("//textarea[@name='text']");
    }  
    get Royltyamountcheckbox(){
        return $("//input[@type='checkbox']");
    } 
    get Royltyamount(){
        return $("//input[@name='royelty']");
    } 
    get Addattributes(){
        return $("//button[contains(text(),'Add attributes')]");
    } 
    get EnterPname1(){
        return $("//input[@name='attributes.0.trait_type']");
    }
    get EnterValue1(){
        return $("//input[@name='attributes.0.value']");
    }
    get EnterPname2(){
        return $("//input[@name='attributes.1.trait_type']");
    }
    get EnterValue2(){
        return $("//input[@name='attributes.1.value']");
    }
    get Add(){
        return $("//button[contains(text(),'+ Add')]");
    }
    get createsubmit(){
        return $("//button[@value='Submit']");
    } 
     
    
    

    // ACTIONS
    async clickCreate(){
        await super.Doclick(this.Create);
    }
    async clickCreateNow(){
        await super.Doclick(this.CreateNow);
    }
    async Entertitle(value){
        await super.setvalue(this.Title,value);
    }
    async EnterAuthorname(value){
        await super.setvalue(this.Authorname,value);
    }
    async EnterPrice(value){
        await super.setvalue(this.EnterPrice,value);
    }
    async SelectChooseCategory(dropdownvalue){
        await super.selectdropdown(this.Choosecategory,dropdownvalue);

    }
    async upload(){
        const filepath="C:/Users/dashs/OneDrive/Pictures/Krishna.jpg"
        const remoteFilePath = await super.uploadphoto(filepath)
        await super.setvalue(this.Choosefile,remoteFilePath);
        await browser.pause(2000);
    }
    async Writedescription(value){
        await super.setvalue(this.Description,value);
    }
    async Royltyamountcheckboxclick(){
        await super.Doclick(this.Royltyamountcheckbox);
    }
    async RoyltyAmount(value){
        await super.setvalue(this.Royltyamount,value);
    }
    async clickAddAttributes(){
        await super.Doclick(this.Addattributes);
    } 
    async EneterProperties1(value){
        await super.setvalue(this.EnterPname1,value);
    }   
    async EneterProperties2(value){
        await super.setvalue(this.EnterPname2,value);
    } 
    async Entervalue1(value){
        await super.setvalue(this.EnterValue1,value);
    }
    async Entervalue2(value){
        await super.setvalue(this.EnterValue2,value);
    }
    async clickAdd(){
        await super.Doclick(this.Add);
    }
    async clickCreate(){
        await super.Doclick(this.createsubmit);
    }
}
module.exports=new createpage();