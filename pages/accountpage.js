const basepge = require("./basepage")
class accountpage extends basepge{
    //LOCATORS
    get account(){
        return $("//button[contains(text(),'Account')]");
    }
    get myWallet(){
        return $("//button[contains(text(),'My Wallet')]");
    }
    get myTransactions(){
        return $("//button[contains(text(),'My Transactions')]");
    }
    get myCollections(){
        return $("//button[@id='mui-p-64208-T-3']");
    }
    get myFavourites(){
        return $("//button[@id='mui-p-64208-T-4']");
    }
    get Document(){
        return $("//button[@id='mui-p-64208-T-5']");
    } 
    //ACTIONS
    async clickaccount(){
        await super.Doclick(this.account);
    }
    async clickmyWallet(){
        await super.Doclick(this.myWallet);
    }
    async clickmyTransactions(){
        await super.Doclick(this.myTransactions);
    }
    async clickmyCollections(){
        await super.Doclick(this.myCollections);
    }
    async clickmyFavourites(){
        await super.Doclick(this.myFavourites);
    }
    async clickDocument(){
        await super.Doclick(this.Document);
    }
    
}
module.exports=new accountpage();