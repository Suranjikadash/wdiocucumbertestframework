Feature: NFT website

     Scenario: Open NFT and create non fungible token

         Given Open the browser and select create
         When select create single NFT
         And enter title and author name
         And enter price
         When choose category
         When choose file to upload
         And write description
         And give royalty amount
         When add attributes
         Then select Create